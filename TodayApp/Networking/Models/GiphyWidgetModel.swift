//
//  GiphyWidgetModel.swift
//  TodayApp
//
//  Created by Maximilian Seiferth on 28.06.20.
//  Copyright © 2020 Maximilian Seiferth. All rights reserved.
//

import UIKit

struct Images_Fixed_Height: Codable {
    let url: String
    let width: String
    let height: String
    let size: String
    let mp4: String
    let mp4_size: String
    let webp: String
    let webp_size: String
}

struct Images: Codable {
    let fixed_height: Images_Fixed_Height
}

struct User: Codable {
    let avatar_url: String
    let banner_url: String
    let profile_url: String
    let username: String
    let display_name: String
}

struct GIF: Codable {
    let type: String
    let id: String
    let slug: String
    let url: String
    let bitly_url: String
    let embed_url: String
    let username: String
    let source: String
    let rating: String
    let user: User?
    let source_tld: String?
    let source_post_url: String?
    let update_datetime: String?
    let create_datetime: String?
    let import_datetime: String?
    let trending_datetime: String?
    let images: Images
    let title: String
}

struct Pagination: Codable {
    let offset: Int
    let total_count: Int
    let count: Int
}

struct Meta: Codable {
    let msg: String
    let status: Int
    let response_id: String
}

struct GiphyMain: Codable {
    let data: [GIF]
    let pagination: Pagination
    let meta: Meta
}
