//
//  WeatherWidgetModel.swift
//  TodayApp
//
//  Created by Maximilian Seiferth on 06.06.20.
//  Copyright © 2020 Maximilian Seiferth. All rights reserved.
//
import Foundation

enum WeatherDescription: String, Codable {
    case clearSky = "clear sky"
    case fewClouds = "few clouds"
    case scatteredClouds = "scattered clouds"
    case brokenClouds = "broken clouds"
    case showerRain = "shower rain"
    case rain = "rain"
    case thunderstorm = "thunderstorm"
    case snow = "snow"
    case mist = "mist"
}


struct Coordinates: Codable {
    let lon: Float
    let lat: Float
}

struct Weather: Codable {
    let id: Int
    let main: String
    let description: String
}

struct MainWeatherData: Codable {
    let temp: Float
    let feels_like: Float
    let temp_min: Float
    let temp_max: Float
    let pressure: Int
    let humidity: Int
}

struct Wind: Codable {
    let speed: Float
    let deg: Int
}

struct WeatherData: Codable {
    let coord: Coordinates
    let weather: [Weather]
    let main: MainWeatherData
    let wind: Wind
}

struct WeatherWidget: Codable {
    let EndpointName: String
    let EndpointVersion: String
    let data: WeatherData
}
