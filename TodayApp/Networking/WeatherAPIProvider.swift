//
//  WeatherAPIProvider.swift
//  TodayApp
//
//  Created by Maximilian Seiferth on 08.07.20.
//  Copyright © 2020 Maximilian Seiferth. All rights reserved.
//

import Foundation
import CoreLocation
import Moya

public enum WeatherAPI {
    case currentWeatherByCoordinates(location: CLLocation)
}

extension WeatherAPI: TargetType {
    public var baseURL: URL {
        return URL(string: "https://api.openweathermap.org")!
    }
    
    public var path: String {
        switch self {
        case .currentWeatherByCoordinates:
            return "/data/2.5/weather"
        }
    }
    
    public var method: Moya.Method {
        return .get
    }
    
    public var sampleData: Data {
        return Data()
    }
    
    public var task: Task {
        switch self {
        case .currentWeatherByCoordinates(let location):
            return .requestParameters(parameters: ["lat": location.coordinate.latitude, "lon": location.coordinate.longitude, "appid": "0c05517b4fbb5ed33e1d270dd8e8bda2", "units": "metric"], encoding: URLEncoding.default)
        }
    }
    
    public var headers: [String : String]? {
        return nil 
    }
    
    
}
