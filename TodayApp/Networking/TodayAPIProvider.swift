//
//  TodayAPIProvider.swift
//  TodayApp
//
//  Created by Maximilian Seiferth on 06.06.20.
//  Copyright © 2020 Maximilian Seiferth. All rights reserved.
//

import Foundation
import Moya

public enum TodayAPI {
    case dashboard
    case weather
}

extension TodayAPI: TargetType {
    public var baseURL: URL {
        return URL(string: "https://c1594240-eabd-44a5-b828-f78a116964c6.mock.pstmn.io/api/v1")!
    }
    
    public var path: String {
        switch self {
        case .dashboard:
            return "/dashboard"
        case .weather:
            return "/weather"
        }
    }
    
    public var method: Moya.Method {
        switch self {
        case .dashboard:
            return .get
        default:
            return .get
        }
    }
    
    public var sampleData: Data {
        switch self {
        case .dashboard:
            guard let data = Helper.bundleJSONData(filename: "DashboardTestData") else { return Data() }
            return data
        case .weather:
            return Data()
        }
    }
    
    public var task: Task {
        switch self {
        case .dashboard:
            return .requestPlain
        case .weather:
            return .requestPlain
        }
    }
    
    public var headers: [String : String]? {
        switch self {
        default:
            return ["Content-Type": "application/json", "x-api-key":"PMAK-5edbb2c65aeb27004ef7cd35-6207b3a8bf6bf8b99fc541654b29367b03"]
        }
    }
    
    
}
