//
//  GiphyAPIProvider.swift
//  TodayApp
//
//  Created by Maximilian Seiferth on 28.06.20.
//  Copyright © 2020 Maximilian Seiferth. All rights reserved.
//

import UIKit
import Moya

public enum GiphyAPI {
    case trending
}

extension GiphyAPI: TargetType {
    public var baseURL: URL {
        return URL(string: "https://api.giphy.com/v1")!
    }
    
    public var path: String {
        switch self {
        case .trending:
            return "/gifs/trending"
        }
    }
    
    public var method: Moya.Method {
        switch self {
        case .trending:
            return .get
        }
    }
    
    public var sampleData: Data {
        return Data()
    }
    
    public var task: Task {
        switch self {
        case .trending:
            return .requestParameters(parameters: ["api_key": "v0iBU2XW0ADPZpOCFNAndpRDHN5HH1Fj", "limit": "25", "rating":"g"], encoding: URLEncoding.default)
        }
    }
    
    public var headers: [String : String]? {
        switch self {
        case .trending:
            return [:]
        }
    }
}
