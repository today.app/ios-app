//
//  HealthWidget.swift
//  TodayApp
//
//  Created by Maximilian Seiferth on 02.07.20.
//  Copyright © 2020 Maximilian Seiferth. All rights reserved.
//

import UIKit
import HealthKitUI
import HealthKit

class HealthWidget: BaseWidgetCell, Widget {
    static var reuseIdentifier: String = "HealthWidget-reuse-identifier"
    let activityRings = HKActivityRingView()
    let activityLabelStackView = UIStackView()
    let energyActivityLabel = UILabel()
    let trainingActivityLabel = UILabel()
    let standingActivityLabel = UILabel()
    let activitLabelWrapperView = UIView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        fetchHealthData()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension HealthWidget {
    func configure() {
        addSubview(activityRings)
        addSubview(activitLabelWrapperView)
        activitLabelWrapperView.addSubview(activityLabelStackView)
        activityRings.translatesAutoresizingMaskIntoConstraints = false
        activityLabelStackView.translatesAutoresizingMaskIntoConstraints = false
        activitLabelWrapperView.translatesAutoresizingMaskIntoConstraints = false

        activityLabelStackView.alignment = .leading
        activityLabelStackView.axis = .vertical
        activityLabelStackView.distribution = .equalSpacing
        activityLabelStackView.spacing = 5
        activityLabelStackView.addArrangedSubview(energyActivityLabel)
        activityLabelStackView.addArrangedSubview(trainingActivityLabel)
        activityLabelStackView.addArrangedSubview(standingActivityLabel)
        
        energyActivityLabel.font = UIFont.systemFont(ofSize: 25.0, weight: .bold)
        energyActivityLabel.textColor = .systemRed
        energyActivityLabel.text = "--/--CAL"
        
        trainingActivityLabel.font = UIFont.systemFont(ofSize: 25.0, weight: .bold)
        trainingActivityLabel.textColor = .systemYellow
        trainingActivityLabel.text = "--/--MIN"
        
        standingActivityLabel.font = UIFont.systemFont(ofSize: 25.0, weight: .bold)
        standingActivityLabel.textColor = .systemGray3
        standingActivityLabel.text = "--/--HRS"
        

        NSLayoutConstraint.activate([
            activityRings.heightAnchor.constraint(equalToConstant: CGFloat(self.frame.size.height - 20)),
            activityRings.widthAnchor.constraint(equalTo: activityRings.heightAnchor),
            activityRings.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 32),
            activityRings.centerYAnchor.constraint(equalTo: centerYAnchor),
            
            activitLabelWrapperView.leadingAnchor.constraint(equalTo: activityRings.trailingAnchor),
            activitLabelWrapperView.trailingAnchor.constraint(equalTo: trailingAnchor),
            activitLabelWrapperView.topAnchor.constraint(equalTo: topAnchor),
            activitLabelWrapperView.bottomAnchor.constraint(equalTo: bottomAnchor),
            
            activityLabelStackView.centerXAnchor.constraint(equalTo: activitLabelWrapperView.centerXAnchor),
            activityLabelStackView.centerYAnchor.constraint(equalTo: activitLabelWrapperView.centerYAnchor),
        ])
    }
    
    func fetchHealthData() {
        let health = HealthManager.shared
        health.queryHealthDataByInterval(startDate: Date(), endDate: Date()) { summaries, error  in
            if let error = error {
                debugPrint(error.localizedDescription)
                return
            }
            
            guard let summaries = summaries else { return }
            if summaries.count == 1 {
                DispatchQueue.main.async {
                    let summary = summaries[0]
                    let activeEnergyBurned = String(format: "%.0f", summary.activeEnergyBurned.doubleValue(for: .largeCalorie()))
                    let activeEnergyGoal = String(format: "%.0f", summary.activeEnergyBurnedGoal.doubleValue(for: .largeCalorie()))
                    let trainingMinutes = String(format: "%.0f", summary.appleExerciseTime.doubleValue(for: .minute()))
                    let trainingMinutesGoal = String(format: "%.0f", summary.appleExerciseTimeGoal.doubleValue(for: .minute()))
                    let standingMinutes = String(format: "%.0f", summary.appleStandHours.doubleValue(for: .count()))
                    let standingMinutesGoal = String(format: "%.0f", summary.appleStandHoursGoal.doubleValue(for: .count()))
                    self.activityRings.setActivitySummary(summary, animated: true)
                    self.energyActivityLabel.text = activeEnergyBurned + "/" + activeEnergyGoal + "CAL"
                    self.trainingActivityLabel.text = trainingMinutes + "/" + trainingMinutesGoal + "MIN"
                    self.standingActivityLabel.text = standingMinutes + "/" + standingMinutesGoal + "HRS"
                }
            }
        }
    }
    
    func onClickAction(parent: UIViewController) {
        
    }
}
