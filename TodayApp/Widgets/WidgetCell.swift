//
//  WidgetCell.swift
//  TodayApp
//
//  Created by Maximilian Seiferth on 06.06.20.
//  Copyright © 2020 Maximilian Seiferth. All rights reserved.
//

import UIKit

protocol Widget {
    static var reuseIdentifier: String { get }
    func configure()
    func onClickAction(parent: UIViewController)
}
