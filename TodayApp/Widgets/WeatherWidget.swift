//
//  WeatherWidget.swift
//  TodayApp
//
//  Created by Maximilian Seiferth on 05.06.20.
//  Copyright © 2020 Maximilian Seiferth. All rights reserved.
//

import UIKit
import Moya

class WeatherWidgetCell: BaseWidgetCell, Widget {
    static let reuseIdentifier = "WeatherWidget-Reuse-Identifier"
    let weatherIconImageView = UIImageView()
    let avgTemperatureLabel = UILabel()
    let maxTemperatureLabel = UILabel()
    let minTemperatureLabel = UILabel()
    let conditionDescriptionLabel = UILabel()
    
    let labelStackView = UIStackView()
    let highLowTempStackView = UIStackView()
    let subLabelStackView = UIStackView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.configureNotifications()
        self.configure()
        self.fetchData()
    }
    
    func configureNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.fetchData), name: updateLocation, object: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func onClickAction(parent: UIViewController) {
        debugPrint("clicked weather widget")
    }
}

extension WeatherWidgetCell {
    func configure() {
        self.backgroundColor = UIColor(named: "WeatherWidget.Normal")

        addSubview(weatherIconImageView)
        addSubview(labelStackView)

        highLowTempStackView.axis = .vertical
        highLowTempStackView.alignment = .center
        highLowTempStackView.distribution = .equalSpacing
        highLowTempStackView.spacing = 15.0
        highLowTempStackView.addArrangedSubview(maxTemperatureLabel)
        highLowTempStackView.addArrangedSubview(minTemperatureLabel)
        
        subLabelStackView.axis = .horizontal
        subLabelStackView.alignment = .center
        subLabelStackView.distribution = .equalSpacing
        subLabelStackView.spacing = 12.0
        subLabelStackView.addArrangedSubview(avgTemperatureLabel)
        subLabelStackView.addArrangedSubview(highLowTempStackView)
        
        labelStackView.axis = .vertical
        labelStackView.alignment = .center
        labelStackView.distribution = .equalSpacing
        labelStackView.addArrangedSubview(subLabelStackView)
        labelStackView.addArrangedSubview(conditionDescriptionLabel)
                
        weatherIconImageView.translatesAutoresizingMaskIntoConstraints = false
        avgTemperatureLabel.translatesAutoresizingMaskIntoConstraints = false
        maxTemperatureLabel.translatesAutoresizingMaskIntoConstraints = false
        minTemperatureLabel.translatesAutoresizingMaskIntoConstraints = false
        conditionDescriptionLabel.translatesAutoresizingMaskIntoConstraints = false
        labelStackView.translatesAutoresizingMaskIntoConstraints = false
        
        avgTemperatureLabel.font = UIFont.systemFont(ofSize: 65, weight: .thin)
        avgTemperatureLabel.textColor = .white
        avgTemperatureLabel.text = "--°C"
        
        maxTemperatureLabel.font = UIFont.boldSystemFont(ofSize: 12)
        maxTemperatureLabel.textColor = .white
        maxTemperatureLabel.text = "--°C"

        minTemperatureLabel.font = UIFont.systemFont(ofSize: 12, weight: .thin)
        minTemperatureLabel.textColor = .white
        minTemperatureLabel.text = "--°C"
        
        weatherIconImageView.image = UIImage(systemName: "sparkles", withConfiguration: UIImage.SymbolConfiguration(pointSize: 50, weight: .thin, scale: .medium))
        weatherIconImageView.tintColor = .white

        NSLayoutConstraint.activate([
            weatherIconImageView.centerYAnchor.constraint(equalTo: safeAreaLayoutGuide.centerYAnchor),
            weatherIconImageView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor, constant: 32),
            weatherIconImageView.heightAnchor.constraint(equalToConstant: 78),
            weatherIconImageView.widthAnchor.constraint(greaterThanOrEqualToConstant: 84),
            
            labelStackView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor, constant: -25),
            labelStackView.centerYAnchor.constraint(equalTo: safeAreaLayoutGuide.centerYAnchor)
        ])
    }
    
    @objc func fetchData() {
        let apiProvider = MoyaProvider<WeatherAPI>()
        apiProvider.request(.currentWeatherByCoordinates(location: LocationManager.shared.location!)) { [weak self] response in
            guard let self = self else { return }

            switch response {
            case .success(let response):
                do {
                    let filteredResponse = try response.filterSuccessfulStatusCodes()
                    let weatherData = try filteredResponse.map(WeatherData.self)
                    self.setupData(weatherData: weatherData)
                } catch {
                    debugPrint(error.localizedDescription)
                }
            case .failure(let error):
                debugPrint(error.localizedDescription)
            }
        }
    }
    
    func setupData(weatherData weather: WeatherData) {
        self.avgTemperatureLabel.text = String(Int(weather.main.temp)) + "°C"
        self.maxTemperatureLabel.text = String(Int(weather.main.temp_max)) + "°C"
        self.minTemperatureLabel.text = String(Int(weather.main.temp_min)) + "°C"
        
        switch weather.weather[0].id {
        case 800:
            self.weatherIconImageView.image = UIImage(systemName: "sun.max", withConfiguration: UIImage.SymbolConfiguration(pointSize: 65, weight: .thin, scale: .medium))
        case 801:
            self.weatherIconImageView.image = UIImage(systemName: "cloud.sun.fill", withConfiguration: UIImage.SymbolConfiguration(pointSize: 65, weight: .thin, scale: .medium))
        case 802:
            self.weatherIconImageView.image = UIImage(systemName: "cloud.fill", withConfiguration: UIImage.SymbolConfiguration(pointSize: 65, weight: .thin, scale: .medium))
        case 803:
            self.weatherIconImageView.image = UIImage(systemName: "smoke.fill", withConfiguration: UIImage.SymbolConfiguration(pointSize: 65, weight: .thin, scale: .medium))
            self.backgroundColor = UIColor(named: "WeatherWidget.Rain")
        case 200...299:
            self.weatherIconImageView.image = UIImage(systemName: "cloud.bolt.rain.fill", withConfiguration: UIImage.SymbolConfiguration(pointSize: 65, weight: .thin, scale: .medium))
            self.backgroundColor = UIColor(named: "WeatherWidget.Rain")
        case 300...399:
            self.weatherIconImageView.image = UIImage(systemName: "cloud.drizzle.fill", withConfiguration: UIImage.SymbolConfiguration(pointSize: 65, weight: .thin, scale: .medium))
            self.backgroundColor = UIColor(named: "WeatherWidget.Rain")
        case 500...599:
            self.weatherIconImageView.image = UIImage(systemName: "cloud.rain.fill", withConfiguration: UIImage.SymbolConfiguration(pointSize: 65, weight: .thin, scale: .medium))
            self.backgroundColor = UIColor(named: "WeatherWidget.Rain")
        case 600...699:
            self.weatherIconImageView.image = UIImage(systemName: "cloud.snow.fill", withConfiguration: UIImage.SymbolConfiguration(pointSize: 65, weight: .thin, scale: .medium))
            self.backgroundColor = UIColor(named: "WeatherWidget.Rain")
        case 700...799:
            self.weatherIconImageView.image = UIImage(systemName: "cloud.fog.fill", withConfiguration: UIImage.SymbolConfiguration(pointSize: 65, weight: .thin, scale: .medium))
            self.backgroundColor = UIColor(named: "WeatherWidget.Rain")
        default:
            self.weatherIconImageView.image = UIImage(systemName: "zzz", withConfiguration: UIImage.SymbolConfiguration(pointSize: 65, weight: .thin, scale: .medium))
            self.backgroundColor = UIColor(named: "WeatherWidget.Normal")
        }
    }
}
