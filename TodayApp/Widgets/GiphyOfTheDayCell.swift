//
//  GiphyOfTheDayCell.swift
//  TodayApp
//
//  Created by Maximilian Seiferth on 27.06.20.
//  Copyright © 2020 Maximilian Seiferth. All rights reserved.
//

import UIKit
import Moya
import SwiftyGif
import SafariServices

class GiphyOfTheDayCell: BaseWidgetCell, Widget {
    static var reuseIdentifier: String = "Giphyoftheday-Reuse-Identifier"
    let giphyAPI = MoyaProvider<GiphyAPI>()
    var randomGiphyNumber = -1
    var giphyData: GiphyMain? {
        didSet {
            self.setGif()
        }
    }
    
    let gifImageView = UIImageView()
    let opacityLayer = UIView()
    let labelStackView = UIStackView()
    let displayNameLabel = UILabel()
    let usernameLabel = UILabel()
    let headlineStackView = UIStackView()
    let headlineLogo = UIImageView()
    let headlineLabel = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.loadData()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func onClickAction(parent: UIViewController) {
        if randomGiphyNumber != -1 {
            guard let giphyData = self.giphyData else { return }
            let giphy = giphyData.data[randomGiphyNumber]
            
            let safariVC = SFSafariViewController(url: URL(string: giphy.bitly_url)!)
            parent.present(safariVC, animated: true)
        }
        debugPrint("Clicked Giphy Cell")
    }
}

extension GiphyOfTheDayCell {
    func configure() {
        addSubview(gifImageView)
        gifImageView.addSubview(opacityLayer)
        gifImageView.addSubview(labelStackView)
        gifImageView.addSubview(headlineStackView)
        
        gifImageView.translatesAutoresizingMaskIntoConstraints = false
        gifImageView.layer.cornerRadius = self.layer.cornerRadius
        gifImageView.layer.masksToBounds = true
        
        opacityLayer.translatesAutoresizingMaskIntoConstraints = false
        opacityLayer.layer.cornerRadius = self.layer.cornerRadius
        opacityLayer.layer.masksToBounds = true
        opacityLayer.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.4)
        
        labelStackView.translatesAutoresizingMaskIntoConstraints = false
        labelStackView.alignment = .leading
        labelStackView.axis = .vertical
        labelStackView.addArrangedSubview(displayNameLabel)
        labelStackView.addArrangedSubview(usernameLabel)
        
        displayNameLabel.font = UIFont.systemFont(ofSize: 15.0, weight: .heavy)
        displayNameLabel.textColor = .white
        
        usernameLabel.font = UIFont.systemFont(ofSize: 12.0, weight: .light)
        usernameLabel.textColor = .white
        
        headlineStackView.translatesAutoresizingMaskIntoConstraints = false
        headlineStackView.alignment = .leading
        headlineStackView.axis = .horizontal
        headlineStackView.spacing = 12.0
        headlineStackView.addArrangedSubview(headlineLogo)
        headlineStackView.addArrangedSubview(headlineLabel)
        
        headlineLogo.image = UIImage(named: "GiphyWidget.Logo")
        headlineLabel.text = "Trending Giphy"
        headlineLabel.font = UIFont.systemFont(ofSize: 25.0, weight: .heavy)
        headlineLabel.textColor = .white
        
        
        NSLayoutConstraint.activate([
            gifImageView.leadingAnchor.constraint(equalTo: leadingAnchor),
            gifImageView.trailingAnchor.constraint(equalTo: trailingAnchor),
            gifImageView.topAnchor.constraint(equalTo: topAnchor),
            gifImageView.bottomAnchor.constraint(equalTo: bottomAnchor),
            
            opacityLayer.leadingAnchor.constraint(equalTo: gifImageView.leadingAnchor),
            opacityLayer.trailingAnchor.constraint(equalTo: gifImageView.trailingAnchor),
            opacityLayer.topAnchor.constraint(equalTo: gifImageView.topAnchor),
            opacityLayer.bottomAnchor.constraint(equalTo: gifImageView.bottomAnchor),
            
            labelStackView.leadingAnchor.constraint(equalTo: gifImageView.leadingAnchor, constant: 15.0),
            labelStackView.bottomAnchor.constraint(equalTo: gifImageView.bottomAnchor, constant: -17.0),
            
            headlineStackView.leadingAnchor.constraint(equalTo: gifImageView.leadingAnchor, constant: 15.0),
            headlineLabel.topAnchor.constraint(equalTo: gifImageView.topAnchor, constant: 20.0),
        ])
    }
    
    func setGif() {
        randomGiphyNumber = Int.random(in: 0..<25)
        guard let giphyData = self.giphyData else { return }
        let randomGiphy = giphyData.data[randomGiphyNumber]
        self.gifImageView.setGifFromURL(URL(string: randomGiphy.images.fixed_height.url)!, manager: .defaultManager, loopCount: -1, levelOfIntegrity: .default, session: .shared, showLoader: true, customLoader: nil)
        
        guard let user = randomGiphy.user else { return }
        self.displayNameLabel.text = user.display_name
        self.usernameLabel.text = "@" + user.username
    }
    
    func loadData() {
        self.giphyAPI.request(.trending) { result in
            switch result {
            case .success(let response):
                do {
                    let filteredResponse = try response.filterSuccessfulStatusCodes()
                    self.giphyData = try filteredResponse.map(GiphyMain.self)
                } catch {
                    debugPrint(error.localizedDescription)
                }
            case .failure(let error):
                debugPrint(error.localizedDescription)
            }
        }
    }
}
