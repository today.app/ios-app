//
//  BaseWidgetCell.swift
//  TodayApp
//
//  Created by Maximilian Seiferth on 27.06.20.
//  Copyright © 2020 Maximilian Seiferth. All rights reserved.
//

import UIKit

class BaseWidgetCell: UICollectionViewCell {
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.configureBaseCell()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension BaseWidgetCell {
    func configureBaseCell() {
        self.backgroundColor = UIColor(named: "TravelTimesWidget.BackgroundColor")
        self.layer.masksToBounds = false
        self.layer.cornerRadius = 11.0
        
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOpacity = 4
        self.layer.shadowOffset = .zero
        self.layer.shadowRadius = 2
    }
}
