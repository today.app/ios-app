//
//  TravelTimeWidget.swift
//  TodayApp
//
//  Created by Maximilian Seiferth on 15.06.20.
//  Copyright © 2020 Maximilian Seiferth. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import EventKit

class TravelTimesWidget: BaseWidgetCell, Widget {
    static let reuseIdentifier = "TravelTimeWidget-Reuse-Identifier"
    
    let iconsStackView = UIStackView()
    let destinationIconStackView = UIStackView()
    let destinationIcon = UIImageView()
    let destinationLabel = UILabel()
    let startIcon = UIImageView()
    let arrowIcon = UIImageView()
    
    let etaLabel = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.configureNotifications()
        self.configure()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    func configureNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.receivedLocationUpdate(notification:)), name: updateLocation, object: nil)
    }
    
    func onClickAction(parent: UIViewController) {
        debugPrint("Clicked Travel Time Widget")
    }
}

extension TravelTimesWidget {
    @objc func receivedLocationUpdate(notification: NSNotification) {
        guard let location = LocationManager.shared.location else { return }
        debugPrint("New Location received:", location)
        self.calculateETA(currentLocation: location)
    }
    
    func getNextCalendarEntry(completion: @escaping ([EKEvent]?) -> Void) {
        switch CalendarManager.shared.authorizationStatus() {
        case .authorized:
            CalendarManager.shared.fetchCalendarData() { events in
                guard let events = events else {
                    completion(nil)
                    return
                }
                var filteredEvents: [EKEvent]? = nil
                for event in events {
                    if event.structuredLocation != nil {
                        filteredEvents?.append(event)
                    }
                }
                completion(filteredEvents)
            }
            return
        case .denied:
            // Do nothing
            return
        case .notDetermined:
            CalendarManager.shared.authorizeForEvents() { error in
                if let error = error {
                    debugPrint(error.localizedDescription)
                }
            }
            return
        case .restricted:
            //Do nothing
            return
        default:
            return
        }
    }
}

extension TravelTimesWidget {
    enum Destination {
        case home
        case work
        case calendar
    }
    
    func configure() {
        addSubview(iconsStackView)
        addSubview(etaLabel)
        
        destinationIconStackView.translatesAutoresizingMaskIntoConstraints = false
        iconsStackView.translatesAutoresizingMaskIntoConstraints = false
        etaLabel.translatesAutoresizingMaskIntoConstraints = false

        destinationIconStackView.axis = .vertical
        destinationIconStackView.distribution = .equalSpacing
        destinationIconStackView.alignment = .center
        destinationIconStackView.addArrangedSubview(destinationIcon)
        destinationIconStackView.addArrangedSubview(destinationLabel)
        
        iconsStackView.axis = .horizontal
        iconsStackView.distribution = .equalSpacing
        iconsStackView.alignment = .center
        iconsStackView.addArrangedSubview(startIcon)
        iconsStackView.addArrangedSubview(arrowIcon)
        iconsStackView.addArrangedSubview(destinationIconStackView)
        
        etaLabel.textColor = .white
        etaLabel.text = "-- min"
        etaLabel.font = UIFont.systemFont(ofSize: 45, weight: .light)
        
        arrowIcon.image = UIImage(systemName: "arrow.right", withConfiguration: UIImage.SymbolConfiguration(pointSize: 40, weight: .light, scale: .medium))
        arrowIcon.tintColor = .white
        
        startIcon.image = UIImage(systemName: "car", withConfiguration: UIImage.SymbolConfiguration(pointSize: 40, weight: .light, scale: .medium))
        startIcon.tintColor = .white
        
        destinationIcon.image = UIImage(systemName: "house", withConfiguration: UIImage.SymbolConfiguration(pointSize: 40, weight: .light, scale: .medium))
        destinationIcon.tintColor = .white
        
        destinationLabel.text = "Home"
        destinationLabel.textColor = .white
        
        NSLayoutConstraint.activate([
            iconsStackView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor, constant: 32),
            iconsStackView.centerYAnchor.constraint(equalTo: centerYAnchor),
            iconsStackView.heightAnchor.constraint(equalToConstant: 70),
            
            etaLabel.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor, constant: -28),
            etaLabel.centerYAnchor.constraint(equalTo: iconsStackView.centerYAnchor),
        ])
    }
    
    func setIcons(destination: Destination) {
        switch destination {
        case .home:
            self.destinationIcon.image = UIImage(systemName: "house", withConfiguration: UIImage.SymbolConfiguration(pointSize: 40, weight: .light, scale: .medium))
            self.destinationLabel.text = "Home"
        case .work:
            self.destinationIcon.image = UIImage(systemName: "briefcase.fill", withConfiguration: UIImage.SymbolConfiguration(pointSize: 40, weight: .light, scale: .medium))
            self.destinationLabel.text = "Work"
        case .calendar:
            return
        }
    }
    
    func calculateETA(currentLocation: CLLocation) {
        let calendarEvents = self.getNextCalendarEntry() { events in
            
        }
        
        let places: [String: CLLocation] =
            [
                "home":
                    CLLocation(latitude: CLLocationDegrees(exactly: 50.788996)!, longitude: CLLocationDegrees(exactly: 7.0336513)!),
                "work":
                    CLLocation(latitude: CLLocationDegrees(exactly: 50.7096802)!, longitude: CLLocationDegrees(exactly: 7.1271543)!)
            ]
        
        var destination: CLLocation = places["home"]!
        
        let distanceFromHome = currentLocation.distance(from: places["home"]!)
        let distanceFromWork = currentLocation.distance(from: places["work"]!)
        debugPrint("Distance from work: \(distanceFromWork)m, Distance from home: \(distanceFromHome)m")
        
        if distanceFromHome <= distanceFromWork && distanceFromWork >= 500 {
            destination = places["work"]!
            setIcons(destination: .work)
            debugPrint("Destination: Work")
        } else if distanceFromWork <= distanceFromHome && distanceFromHome >= 500 {
            destination = places["home"]!
            setIcons(destination: .home)
            debugPrint("Destination: Home")
        } else {
            destination = places["home"]!
            setIcons(destination: .home)
            debugPrint("Destination: Home")
        }
        
        let startPoint = MKMapItem(placemark: MKPlacemark(coordinate: currentLocation.coordinate))
        let destinationPoint = MKMapItem(placemark: MKPlacemark(coordinate: destination.coordinate))
        
        let request = MKDirections.Request()
        request.source = startPoint
        request.destination = destinationPoint
        let directions = MKDirections(request: request)
        directions.calculateETA() { response, error in
            if let error = error {
                debugPrint("ETA: ", error.localizedDescription)
                return
            }
            guard let response = response else { return }
            self.etaLabel.text = response.expectedTravelTime.stringMinuteFormatted()
        }
    }
}


extension TimeInterval {
    // builds string in app's labels format 00:00.0
    func stringMinuteFormatted() -> String {
        let interval = Int(self)
        let minutes = (interval / 60) % 60
        let hours = (interval / 60) / 60
        debugPrint(hours)
        if hours == 0 {
            return String(format: "%02d min", minutes)
        } else {
            return String(format: "%02d:%02d h", hours, minutes)
        }
    }
}
