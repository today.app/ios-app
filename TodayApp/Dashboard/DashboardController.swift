//
//  DashboardController.swift
//  TodayApp
//
//  Created by Maximilian Seiferth on 01.06.20.
//  Copyright © 2020 Maximilian Seiferth. All rights reserved.
//

import Foundation
import Moya

enum CollectionWidgetStyle: String, Codable {
    case small = "smallWidgetCollection"
    case large = "largeWidgetCollection"
}

enum WidgetType: String, Codable {
    case weather = "WeatherWidget"
    case travelTimes = "TravelTimesWidget"
    case jodel = "JodelWidget"
    case giphy = "GiphyWidget"
    case spotify = "SpotifyWidget"
    case news = "NewsWidget"
    case ninegag = "9GagWidget"
    case health = "HealthWidget"
}

class DashboardController {
    struct Widget: Hashable, Codable {
        let widgetName: String
        let widgetType: WidgetType
        
        let identifier = UUID()
        func hash(into hasher: inout Hasher) {
            hasher.combine(identifier)
        }
    }

    
    struct DashboardCollection: Hashable, Codable {
        let widgetCount: Int
        let collectionWidgetStyle: CollectionWidgetStyle
        let widgets: [Widget]
        
        let identifier = UUID()
        func hash(into hasher: inout Hasher) {
            hasher.combine(identifier)
        }
    }
    
    struct Dashboard: Hashable, Codable {
        let EndpointName: String
        let EndpointVersion: String
        let greetingMessage: String
        let collections: [DashboardCollection]
        
        let identifier = UUID()
        func hash(into hasher: inout Hasher) {
            hasher.combine(identifier)
        }
    }
    
    var dashboardCollection: [DashboardCollection] {
        return _collections
    }
    
    fileprivate var _collections = [DashboardCollection]()

}

extension DashboardController {
    func generateWidgetCollection(completion: @escaping (_ dashboard: Dashboard) -> Void) {
        let provider = MoyaProvider<TodayAPI>(stubClosure: MoyaProvider.immediatelyStub)
        provider.request(.dashboard) { [weak self] result in
            guard let self = self else { return }
            
            switch result {
            case .success(let response):
                do {
                    let filteredResponse = try response.filterSuccessfulStatusCodes()
                    let dashboard = try filteredResponse.map(DashboardController.Dashboard.self)
                    self._collections = dashboard.collections
                    completion(dashboard)
                } catch {
                    debugPrint(error.localizedDescription)
                }
                break
            case .failure(let error):
                debugPrint(error.localizedDescription)
                break
            }
        }
    }
}
