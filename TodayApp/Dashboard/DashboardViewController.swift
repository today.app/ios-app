//
//  ViewController.swift
//  TodayApp
//
//  Created by Maximilian Seiferth on 01.06.20.
//  Copyright © 2020 Maximilian Seiferth. All rights reserved.
//

import UIKit
import CoreLocation

class DashboardViewController: UIViewController {
    var collectionView: UICollectionView! = nil
    var dataSource: UICollectionViewDiffableDataSource<DashboardController.DashboardCollection, DashboardController.Widget>! = nil
    var currentSnapshot: NSDiffableDataSourceSnapshot<DashboardController.DashboardCollection, DashboardController.Widget>! =  nil
    let dashboardController = DashboardController()
    static let titleElementKind = "title-element-kind"
    var greetingMessage: String = "Guten Tag!"

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.view.backgroundColor = UIColor(named: "Dashboard.BackgroundColor")
        dashboardController.generateWidgetCollection() { result in
            self.greetingMessage = result.greetingMessage
            self.configureHierachy()
            self.configureDataSource()
            self.configureNotifications()
            self.reloadData()
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
          return .lightContent
    }
    
    func configureNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.receivedLocationUpdate(notification:)), name: updateLocation, object: nil)
    }
}

extension DashboardViewController {
    @objc func receivedLocationUpdate(notification: NSNotification) {
        //guard let location = LocationManager.shared.location else { return }
        //self.reloadData()
    }
}

extension DashboardViewController {
    func createLayout() -> UICollectionViewLayout {
        let sectionProvider = {(sectionIndex: Int, layoutEnvironment: NSCollectionLayoutEnvironment) -> NSCollectionLayoutSection? in
            switch self.dashboardController.dashboardCollection[sectionIndex].collectionWidgetStyle {
            case .small:
                return self.createSmallWidgetSection(sectionIndex: sectionIndex)
            case .large:
                return self.createLargeWidgetSection(sectionIndex: sectionIndex)
            }
        }
        
        let config = UICollectionViewCompositionalLayoutConfiguration()
        config.interSectionSpacing = 21.0
        
        let layout = UICollectionViewCompositionalLayout(sectionProvider: sectionProvider, configuration: config)
        return layout
    }
    
    func createSmallWidgetSection(sectionIndex index: Int) -> NSCollectionLayoutSection {
        let itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: .fractionalHeight(1.0))
        let item = NSCollectionLayoutItem(layoutSize: itemSize)
                
        let groupSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: .estimated(127))
        let group = NSCollectionLayoutGroup.vertical(layoutSize: groupSize, subitems: [item])
        
        let section = NSCollectionLayoutSection(group: group)
        section.orthogonalScrollingBehavior = .none
        section.interGroupSpacing = 21.0
        section.contentInsets = NSDirectionalEdgeInsets(top: 0, leading: 10, bottom: 0, trailing: 10)
        
        if index == 0 {
            let titleSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: .estimated(44))
            let titleSupplementary = NSCollectionLayoutBoundarySupplementaryItem(layoutSize: titleSize, elementKind: DashboardViewController.titleElementKind, alignment: .top)

            section.boundarySupplementaryItems = [titleSupplementary]
        }
        
        return section
    }
    
    func createLargeWidgetSection(sectionIndex index: Int) -> NSCollectionLayoutSection {
        let itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: .fractionalHeight(1.0))
        let item = NSCollectionLayoutItem(layoutSize: itemSize)
                
        let groupSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: .estimated(256))
        let group = NSCollectionLayoutGroup.vertical(layoutSize: groupSize, subitems: [item])
        
        let section = NSCollectionLayoutSection(group: group)
        section.orthogonalScrollingBehavior = .none
        section.interGroupSpacing = 0
        section.contentInsets = NSDirectionalEdgeInsets(top: 0, leading: 10, bottom: 0, trailing: 10)
        

        if index == 0 {
            let titleSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: .estimated(44))
            let titleSupplementary = NSCollectionLayoutBoundarySupplementaryItem(layoutSize: titleSize, elementKind: DashboardViewController.titleElementKind, alignment: .top)
            section.boundarySupplementaryItems = [titleSupplementary]
        }
        return section
    }
}

extension DashboardViewController {
    func configureHierachy() {
        collectionView = UICollectionView(frame: .zero, collectionViewLayout: createLayout())
        collectionView.delegate = self
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.backgroundColor = .clear
        collectionView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 60.0, right: 0)
        collectionView.alwaysBounceVertical = true
        view.addSubview(collectionView)
        NSLayoutConstraint.activate([
            collectionView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            collectionView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            collectionView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 20.0),
            collectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
        collectionView.register(WeatherWidgetCell.self, forCellWithReuseIdentifier: WeatherWidgetCell.reuseIdentifier)
        collectionView.register(TravelTimesWidget.self, forCellWithReuseIdentifier: TravelTimesWidget.reuseIdentifier)
        collectionView.register(GiphyOfTheDayCell.self, forCellWithReuseIdentifier: GiphyOfTheDayCell.reuseIdentifier)
        collectionView.register(HealthWidget.self, forCellWithReuseIdentifier: HealthWidget.reuseIdentifier)
        collectionView.register(DashboardHeaderView.self, forSupplementaryViewOfKind: DashboardViewController.titleElementKind, withReuseIdentifier: DashboardHeaderView.reuseIdentifier)
        
        
    }
    
    func configureDataSource() {
        dataSource = UICollectionViewDiffableDataSource<DashboardController.DashboardCollection, DashboardController.Widget> (collectionView: collectionView) {
               (collectionView: UICollectionView, indexPath: IndexPath, set: DashboardController.Widget) -> UICollectionViewCell? in
               
            switch self.dashboardController.dashboardCollection[indexPath.section].widgets[indexPath.item].widgetType {
                case .weather:
                    return self.configure(WeatherWidgetCell.self, for: indexPath)
                case .travelTimes:
                    return self.configure(TravelTimesWidget.self, for: indexPath)
                case .giphy:
                    return self.configure(GiphyOfTheDayCell.self, for: indexPath)
                case .health:
                    return self.configure(HealthWidget.self, for: indexPath)
                default:
                    return self.configure(WeatherWidgetCell.self, for: indexPath)
            }
        }
           
        dataSource.supplementaryViewProvider = { [weak self] (collectionView: UICollectionView, kind: String, indexPath: IndexPath) -> UICollectionReusableView? in
            guard let self = self, let snapshot = self.currentSnapshot else { return nil }
            if let titleSupplementary = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: DashboardHeaderView.reuseIdentifier, for: indexPath) as? DashboardHeaderView {

               //let setCategory = snapshot.sectionIdentifiers[indexPath.section]
                titleSupplementary.greetingLabel.text = self.greetingMessage
               
                return titleSupplementary
           } else {
               fatalError("Cannot create new supplementary")
           }
        }
    }
    
    func reloadData() {
        currentSnapshot = NSDiffableDataSourceSnapshot<DashboardController.DashboardCollection, DashboardController.Widget>()
        dashboardController.dashboardCollection.forEach {
            let collection = $0
            currentSnapshot.appendSections([collection])
            currentSnapshot.appendItems(collection.widgets)
        }
        dataSource.apply(currentSnapshot, animatingDifferences: false)
    }
        
    func configure<T: Widget>(_ cellType: T.Type, for indexPath: IndexPath) -> T {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellType.reuseIdentifier, for: indexPath) as? T else {
            fatalError("Unable to dequeue \(cellType)")
        }
        
        cell.configure()
        
        return cell
    }
}

extension DashboardViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {

        if section != 0 {
            return CGSize.zero
        } else {
            let flowLayout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
            return flowLayout.headerReferenceSize
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! Widget
        cell.onClickAction(parent: self)
    }
}
