//
//  WidgetController.swift
//  TodayApp
//
//  Created by Maximilian Seiferth on 09.07.20.
//  Copyright © 2020 Maximilian Seiferth. All rights reserved.
//

import Foundation

enum Widgets: CaseIterable {
    case TravelTime
    case Weather
    case GiphyOfTheDay
    case Health
}

class WidgetController {
    var availableWidgets = Widgets.AllCases()
    
    init() {
        
    }
}
