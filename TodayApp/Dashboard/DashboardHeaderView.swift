//
//  DashboardHeaderView.swift
//  TodayApp
//
//  Created by Maximilian Seiferth on 13.06.20.
//  Copyright © 2020 Maximilian Seiferth. All rights reserved.
//

import UIKit

class DashboardHeaderView: UICollectionReusableView {
    let greetingLabel = UILabel()
    let locationLabel = UILabel()
    let locationPinImageView = UIImageView()
    let locationStackView = UIStackView()
    static let reuseIdentifier = "title-supplementary-reuse-identifier"
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configure()
    }
    
    required init?(coder: NSCoder) {
        fatalError()
    }
}

extension DashboardHeaderView {
    func configure() {
        addSubview(greetingLabel)
        addSubview(locationStackView)
        
        greetingLabel.translatesAutoresizingMaskIntoConstraints = false
        //greetingLabel.adjustsFontForContentSizeCategory = true
        locationStackView.translatesAutoresizingMaskIntoConstraints = false
        locationStackView.axis = .horizontal
        locationStackView.alignment = .leading
        locationStackView.spacing = 5.0
        locationStackView.addArrangedSubview(locationPinImageView)
        locationStackView.addArrangedSubview(locationLabel)
                
        
        let inset = CGFloat(10)
        NSLayoutConstraint.activate([
            greetingLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: inset),
            greetingLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -inset),
            greetingLabel.topAnchor.constraint(equalTo: topAnchor, constant: inset),
            //greetingLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -inset)
            
            locationStackView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: inset),
            locationStackView.topAnchor.constraint(equalTo: greetingLabel.bottomAnchor, constant: 0),
            locationStackView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -25)
        ])
        
        greetingLabel.font = UIFont.systemFont(ofSize: 30.0, weight: .heavy)
        greetingLabel.textColor = .white
        
        locationLabel.font = UIFont.systemFont(ofSize: 18.0, weight: .regular)
        locationLabel.textColor = .white
        locationLabel.text = "---"
        
        locationPinImageView.image = UIImage(systemName: "mappin.circle.fill", withConfiguration: UIImage.SymbolConfiguration(pointSize: 18.0, weight: .medium, scale: .medium))
        locationPinImageView.tintColor = .white
        

        fetchLocation()
    }
    
    func fetchLocation() {
        LocationManager.shared.lookUpCurrentLocation() { placemark in
            self.locationLabel.text = placemark?.locality
        }
    }
}
