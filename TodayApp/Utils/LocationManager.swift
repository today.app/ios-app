//
//  LocationManager.swift
//  TodayApp
//
//  Created by Maximilian Seiferth on 17.06.20.
//  Copyright © 2020 Maximilian Seiferth. All rights reserved.
//

import Foundation
import CoreLocation

class LocationManager: NSObject {
    static let shared = LocationManager()
    
    let locationManager = CLLocationManager()
    var location: CLLocation? {
        didSet {
            locationUpdated()
        }
    }
    
    override private init() {
        super.init()
        locationManager.delegate = self
        guard let storedLocation = self.locationManager.location else {
            self.location = CLLocation(latitude: CLLocationDegrees(exactly: 51.509865)!, longitude: CLLocationDegrees(exactly: -0.118092)!)
            return
        }
        self.location = storedLocation
    }
    
    func locationUpdated() {
        NotificationCenter.default.post(name: updateLocation, object: nil)
        debugPrint("Location update sent to receivers...")
    }
    
    public func startSignificantLocationMonitor() {
        locationManager.requestWhenInUseAuthorization()
        locationManager.requestLocation()

        if !CLLocationManager.significantLocationChangeMonitoringAvailable() {
            return
        }
        
        locationManager.startMonitoringSignificantLocationChanges()
        debugPrint("Location monitor started...")
    }
}

extension LocationManager: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.last {
            self.location = location
            UserDefaults.standard.set(location: location, forKey: "LocationManager.lastLocation")
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        debugPrint("Location Manager: ", error.localizedDescription)
    }
    
    func lookUpCurrentLocation(completionHandler: @escaping (CLPlacemark?) -> Void ) {
        // Use the last reported location.
        if let lastLocation = self.locationManager.location {
            let geocoder = CLGeocoder()
                
            // Look up the location and pass it to the completion handler
            geocoder.reverseGeocodeLocation(lastLocation, completionHandler: { (placemarks, error) in
                if error == nil {
                    let firstLocation = placemarks?[0]
                    completionHandler(firstLocation)
                }
                else {
                 // An error occurred during geocoding.
                    completionHandler(nil)
                }
            })
        }
        else {
            // No location was available.
            completionHandler(nil)
        }
    }
}
