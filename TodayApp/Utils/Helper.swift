//
//  Helper.swift
//  TodayApp
//
//  Created by Maximilian Seiferth on 09.07.20.
//  Copyright © 2020 Maximilian Seiferth. All rights reserved.
//

import Foundation

struct Helper {
    static func bundleJSONData(filename: String) -> Data? {
        if let path = Bundle.main.path(forResource: filename, ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                return data
            } catch {
                
            }
        }
        return nil
    }
}
