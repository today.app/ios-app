//
//  CalendarManager.swift
//  TodayApp
//
//  Created by Maximilian Seiferth on 09.07.20.
//  Copyright © 2020 Maximilian Seiferth. All rights reserved.
//

import Foundation
import EventKit

class CalendarManager: NSObject {
    static let shared = CalendarManager()
    private var eventStore = EKEventStore()
    
    override init() {
        super.init()
    }
    
    func authorizationStatus() -> EKAuthorizationStatus {
        return EKEventStore.authorizationStatus(for: .event)
    }
    
    func authorizeForEvents(completion: @escaping (Error?) -> Void) {
        eventStore.requestAccess(to: .event) { success, error in
            if let error = error {
                debugPrint(error.localizedDescription)
                completion(error)
            }
            completion(nil)
        }
    }
    
    func fetchCalendarData(comletion: @escaping ([EKEvent]?) -> Void) {
        let predicate = eventStore.predicateForEvents(withStart: Date(), end: Date(), calendars: nil)
        var events: [EKEvent]? = nil
        events = eventStore.events(matching: predicate)
        comletion(events)
    }
}
