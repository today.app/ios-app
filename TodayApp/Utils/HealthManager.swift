//
//  Health.swift
//  TodayApp
//
//  Created by Maximilian Seiferth on 02.07.20.
//  Copyright © 2020 Maximilian Seiferth. All rights reserved.
//

import Foundation
import HealthKit

class HealthManager {
    static let shared = HealthManager()
    var store: HKHealthStore?
    
    init() {
        if HKHealthStore.isHealthDataAvailable() {
            self.store = HKHealthStore()
            checkAuthorizations() { error in
                debugPrint(error?.localizedDescription)
            }
        }
    }
    
    func checkAuthorizations(completion: @escaping (Error?) -> Void) {
        let permissions = Set([HKObjectType.activitySummaryType()])
        
        guard let store = self.store else {
            completion(HKError(.errorDatabaseInaccessible))
            return
        }
        
        store.requestAuthorization(toShare: nil, read: permissions) { (success, error) in
            if !success {
                completion(error)
            }
            completion(nil)
        }
    }
    
    func queryHealthDataByInterval(startDate: Date, endDate: Date, completion: @escaping ([HKActivitySummary]?, Error?) -> Void) {
        let calendar = NSCalendar.current

        let units: Set<Calendar.Component> = [.day, .month, .year, .era]
        
        var startDateComponents = calendar.dateComponents(units, from: startDate)
        startDateComponents.calendar = calendar
        
        var endDateComponents = calendar.dateComponents(units, from: endDate)
        endDateComponents.calendar = calendar
        
        let summariesWithinRange = HKQuery.predicate(forActivitySummariesBetweenStart: startDateComponents, end: endDateComponents)
        
        let query = HKActivitySummaryQuery(predicate: summariesWithinRange) { (query, summariesOrNil, errorOrNil) -> Void in
            guard let summaries = summariesOrNil else {
                completion(nil, errorOrNil)
                return
            }
            
            completion(summaries, nil)
        }
        
        guard let store = self.store else {
            completion(nil, HKError(.errorHealthDataUnavailable))
            return
        }
        store.execute(query)
    }
}
